#lang racket/base
(provide (all-defined-out))

; NSWindowStyleMask
(define NSWindowStyleMaskTitled 1)

; NSBacking
(define NSBackingStoreBuffered 2)